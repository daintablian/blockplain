import React from "react";
import styled from "styled-components";

const Flex1 = styled.div`
  flex-grow: 1;
  flex-basis: 0;
`;
const Flex3 = styled.div`
  flex-grow: 3;
  flex-basis: 0;
`;
const AttackButton = styled.button`
  height: 100%;
  width: 100%;
`;
const TopContainer = styled.div`
  border: 1px solid grey;
  display: flex;
  height: 100%;
  width: 100%;
  opacity: ${props => (props.show ? "1" : "0")};
  flex-direction: column;
  background-color: #49b293;
  transition: all 1s;
  transition-delay: 1s
  // background-color: ${props => (props.ownerFighterIndex > -1 ? "pink" : "")};
`;

export default class FighterInterface extends React.Component {
  constructor(props) {
    super(props);
    this.cubeRef = React.createRef();
    this.state = {
      alteringFighter: false,
      isActive: false
    };
  }
  render() {
    const { fighter, land, focusedLandCoordinates, show } = this.props;
    console.log("land height", land && land.height);
    return (
      <TopContainer show={show} ownerFighterIndex={!!fighter.ownerFighterIndex}>
        <button onClick={() => this.props.handleAlterFighterClick()}>
          alter fighter
        </button>
        <button onClick={() => this.props.handleAlterLandClick()}>
          alter land
        </button>
        <Flex1>
          <AttackButton onClick={() => this.props.onFight(0)}>
            &uarr;
            {fighter.top}
          </AttackButton>
        </Flex1>
        <Flex3 style={{ display: "flex" }}>
          <Flex1>
            <AttackButton onClick={() => this.props.onFight(3)}>
              &larr;
              {fighter.left}
            </AttackButton>
          </Flex1>
          <Flex3 style={{ fontSize: "10px" }}>
            <p>
              fighter
              {fighter.id}:{fighter.x},{fighter.y}${fighter.currency}
            </p>
            {land && (
              <p>
                land
                {land.id}: ${land.currency}
              </p>
            )}
          </Flex3>
          <Flex1>
            <AttackButton onClick={() => this.props.onFight(1)}>
              {fighter.right}
              &rarr;
            </AttackButton>
          </Flex1>
        </Flex3>
        <Flex1>
          <AttackButton onClick={() => this.props.onFight(2)}>
            {fighter.bottom}
            &darr;
          </AttackButton>
        </Flex1>
      </TopContainer>
    );
  }
}
