import React from "react";
import styled, { ThemeProvider, keyframes } from "styled-components";
import FighterInterface from "./fighterInterface";

function createFocusAnimation({
  cubeX,
  cubeY,
  cubeZ,
  focusLiftHeight,
  xCoord,
  yCoord,
  focusedLandCoordinates,
  isFocusAnimated
}) {
  const margin = 5;
  const xDifference = focusedLandCoordinates.x - xCoord;
  const yDifference = focusedLandCoordinates.y - yCoord;

  if (isFocusAnimated)
    return keyframes`
0% {
  transform: translateZ(10px) rotateZ(0deg) rotateX(0deg) rotateY(0deg);
  width: 100px;
}

100% {
  transform: translateZ(${focusLiftHeight +
    xDifference * 70 +
    yDifference *
      70}px) rotateZ(-45deg) rotateX(-45deg) translateX(${xDifference *
      (-30 - margin) +
      yDifference * -70}px) translateY(${xDifference * 100 +
      yDifference * -margin}px) rotateY(360deg);
}
`;
}

const Container = styled.div`
  width: ${props => props.theme.cubeX}px;
  height: ${props => props.theme.cubeY}px;
  position: relative;
  transform-style: preserve-3d;
  animation: ${props => createFocusAnimation(props.theme)} 1s forwards;
  animation-delay: ${props => props.theme.animationSpeed * 0.5}s;
  transform: translateZ(
    ${props => (props.isActive ? 10 : props.theme.additionalHeight)}px
  );
  transition: transform 0.5s;
`;
const CubeFace = styled.div`
  position: absolute;
  border: 1px solid black;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  color: white;
  background: saddlebrown;
`;
const Front = styled(CubeFace)`
  transform: rotateY(0deg) translateZ(0px);
  height: ${props => props.theme.cubeY}px;
  width: ${props => props.theme.cubeX}px;
  background: lightgreen;
`;

const Back = styled(CubeFace)`
  transform: rotateY(180deg) translateZ(${props => props.theme.cubeZ}px);
`;
const Right = styled(CubeFace)`
  transform: rotateY(90deg) translateZ(${props => props.theme.cubeX}px);
  height: ${props => props.theme.cubeY}px;
  width: ${props => props.theme.cubeZ}px;
  transform-origin: left;
`;
const Left = styled(CubeFace)`
  transform: rotateY(90deg) translateZ(0px);
  height: ${props => props.theme.cubeY}px;
  width: ${props => props.theme.cubeZ}px;
  transform-origin: left;
`;
const Top = styled(CubeFace)`
  transform: rotateX(-90deg) translateZ(0px);
  width: ${props => props.theme.cubeX}px;
  height: ${props => props.theme.cubeZ}px;
  transform-origin: top;
`;
const Bottom = styled(CubeFace)`
  transform: rotateX(-90deg) translateZ(${props => props.theme.cubeY}px);
  width: ${props => props.theme.cubeX}px;
  height: ${props => props.theme.cubeZ}px;
  transform-origin: top;
`;

export default class Cube extends React.Component {
  constructor(props) {
    super(props);
    this.cubeRef = React.createRef();
    this.state = {
      alteringFighter: false,
      isActive: false
    };
  }
  render() {
    const { fighter, land, focusedLandCoordinates } = this.props;
    const additionalHeight = land && land.height ? land.height * 10 : 0;
    const landHeight = additionalHeight + this.props.cubeZ;
    const xCoord = Number(fighter.x);
    const yCoord = Number(fighter.y);
    const isCenterFocused =
      focusedLandCoordinates.x === xCoord &&
      focusedLandCoordinates.y === yCoord &&
      0.01;
    const isLeftFocused =
      focusedLandCoordinates.x - 1 === xCoord &&
      focusedLandCoordinates.y === yCoord &&
      1;
    const isRightFocused =
      focusedLandCoordinates.x + 1 === xCoord &&
      focusedLandCoordinates.y === yCoord &&
      0.5;
    const isTopFocused =
      focusedLandCoordinates.x === xCoord &&
      focusedLandCoordinates.y - 1 === yCoord &&
      0.25;
    const isBottomFocused =
      focusedLandCoordinates.x === xCoord &&
      focusedLandCoordinates.y + 1 === yCoord &&
      0.75;
    const speedAndIsFocused =
      isCenterFocused ||
      isBottomFocused ||
      isTopFocused ||
      isLeftFocused ||
      isRightFocused;
    return (
      <ThemeProvider
        theme={{
          cubeX: this.props.cubeX,
          cubeY: this.props.cubeY,
          cubeZ: landHeight,
          additionalHeight: additionalHeight,
          focusLiftHeight: 300,
          animationSpeed: speedAndIsFocused,
          xCoord: xCoord,
          yCoord: yCoord,
          focusedLandCoordinates: focusedLandCoordinates,
          isFocusAnimated: speedAndIsFocused
        }}
      >
        <Container
          ref={this.cubeRef}
          isActive={this.state.isActive && !speedAndIsFocused}
        >
          <Front
            onClick={() => this.props.onFocusClick()}
            onMouseEnter={() => this.setState({ isActive: true })}
            onMouseLeave={() => this.setState({ isActive: false })}
          >
            <FighterInterface {...this.props} show={speedAndIsFocused} />
          </Front>

          <Back />
          <Right />
          <Left />
          <Top />
          <Bottom />
        </Container>
      </ThemeProvider>
    );
  }
}
