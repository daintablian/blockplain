import React from "react";
import styled from "styled-components";

const Window = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  height: 100%;
`;

export default class AlterFighter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      left: props.left,
      right: props.right,
      bottom: props.bottom,
      top: props.top
    };
  }

  render() {
    const { handleAlterFighter, x, y, id, unspentWins } = this.props;
    return (
      <Window>
        <div>
          ID:
          {id}
          <br />
          X:
          {x}
          <br />
          Y:
          {y}
          <br />
          Unspent Wins: {unspentWins}
        </div>
        <form>
          <div>
            <label>left</label>
            <input
              value={this.state.left}
              onChange={e => this.setState({ left: e.target.value })}
            />
          </div>
          <div>
            <label>right</label>
            <input
              value={this.state.right}
              onChange={e => this.setState({ right: e.target.value })}
            />
          </div>
          <div>
            <label>top</label>
            <input
              value={this.state.top}
              onChange={e => this.setState({ top: e.target.value })}
            />
          </div>
          <div>
            <label>bottom</label>
            <input
              value={this.state.bottom}
              onChange={e => this.setState({ bottom: e.target.value })}
            />
          </div>
          <button
            onClick={e => {
              e.preventDefault();
              return handleAlterFighter(
                id,
                this.state.top,
                this.state.right,
                this.state.bottom,
                this.state.left
              );
            }}
          >
            alter
          </button>
        </form>
      </Window>
    );
  }
}
