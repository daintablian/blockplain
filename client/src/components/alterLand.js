import React from "react";
import styled from "styled-components";

const Button = styled.button`
  background-color: ${props => (props.selected ? "blue" : "")};
`;

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      up: false,
      down: false
    };
  }
  render() {
    console.log("alterland", this.props);
    const height = Number(this.props.height);
    return (
      <div>
        Height:
        {this.state.up ? height + 1 : this.state.down ? height - 1 : height}
        <br />
        Currency:
        {this.props.currency}
        <br />
        ID:
        {this.props.id}
        <Button
          selected={this.state.up}
          onClick={() => this.setState({ up: !this.state.up, down: false })}
        >
          up
        </Button>
        <Button
          selected={this.state.down}
          onClick={() => this.setState({ down: !this.state.down, up: false })}
        >
          down
        </Button>
        <button onClick={() => this.props.handleAlterLand(this.state.up)}>
          submit
        </button>
      </div>
    );
  }
}
