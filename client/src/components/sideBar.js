import React from "react";
import styled from "styled-components";

const Container = styled.div`
  position: absolute;
  background-color: rgba(29, 29, 29, 90%);
  right: ${props => (props.show ? "0" : "-200px")};
  top: 0;
  height: 100%;
  width: 200px;
  transition: right 0.3s;
  color: white;
`;

export default props => {
  return <Container show={props.show}>{props.children}</Container>;
};
