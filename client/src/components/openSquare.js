import React from "react";
import styled from "styled-components";
const Square = styled.div`
  width: ${props => props.cubeX}px;
  height: ${props => props.cubeY}px;
  border: 1px solid grey;
  box-sizing: border-box;
`;

export default ({ xCoordinate, yCoordinate, onClick, cubeX, cubeY }) => {
  return (
    <Square cubeX={cubeX} cubeY={cubeY}>
      <div>Click Here</div>
      <div>{xCoordinate}</div>
      <div>{yCoordinate}</div>
      <button onClick={onClick}>Create</button>
    </Square>
  );
};
