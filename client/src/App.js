import React, { Component } from "react";
import Game from "./contracts/Game.json";
import getWeb3 from "./utils/getWeb3";
import Cube from "./components/cube";
import OpenSquare from "./components/openSquare";
import AlterFighter from "./components/alterFighter";
import SideBar from "./components/sideBar";
import AlterLand from "./components/alterLand";
import styled from "styled-components";

import "./App.css";

const BoardContainer = styled.div`
  // margin: -4em 0 0 -4em;
`;

const GameBoard = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  display: flex;
  // transform-origin: top left;
  transition: all 1s;
  transform-style: preserve-3d;
  width: 8em;
  height: 8em;
  margin: -4em 0 0 -4em;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.gameBoard = React.createRef();
  }
  state = {
    web3: null,
    accounts: null,
    contract: null,
    gasUsed: "",
    myFighters: [],
    fighters: [],
    lands: [],
    alteringFighter: false,
    alteringLand: false,
    cubeX: 100,
    cubeY: 100,
    cubeZ: 100,
    focusedLandCoordinates: { x: undefined, y: undefined },
    rotateX: 45,
    rotateY: 0,
    rotateZ: 45,
    translateX: 0,
    translateY: 0,
    translateZ: 0,
    recentX: undefined,
    recentY: undefined
  };

  componentDidMount = async () => {
    try {
      const web3 = await getWeb3();
      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();
      // Get the contract instance.
      const Contract = new web3.eth.Contract(
        Game.abi,
        Game.networks[5777].address
      );
      this.setState(
        {
          web3,
          accounts,
          contract: Contract
        },
        () => this.refreshData()
      );
    } catch (error) {
      alert(
        `Failed to loads web3, accounts, or contract. Check console for details.`,
        error.message
      );
    }
  };
  refreshData() {
    this.getFighters();
    this.getMyFighters();
    this.getLands();
    this.setState({ alteringFighter: false, alteringLand: false });
  }
  formatFighter(object) {
    const fighter = {
      currency: object[0],
      left: object[1],
      right: object[2],
      top: object[3],
      bottom: object[4],
      x: object[5],
      y: object[6],
      id: object[7],
      unspentWins: object[8]
    };
    return fighter;
  }
  getMyFighters() {
    const {
      getFighterCountByOwner,
      getFighterByOwnerListIndex
    } = this.state.contract.methods;
    return getFighterCountByOwner()
      .call()
      .then(fighterCount => {
        let ar = [];
        for (let i = 0; i < fighterCount; i++) {
          ar.push(i);
        }
        return Promise.all(
          ar.map(index => getFighterByOwnerListIndex(index).call())
        );
      })
      .then(crudeFighters =>
        crudeFighters.map(f => {
          return {
            x: f[0],
            y: f[1],
            id: f[2]
          };
        })
      )
      .then(fighters => this.setState({ myFighters: fighters }));
  }
  handleCreateFighter(xCoordinate, yCoordinate) {
    this.state.contract.methods
      .createFighter(xCoordinate, yCoordinate)
      .send({
        from: this.state.accounts[0],
        gas: 5000000
      })
      .then(() => this.refreshData());
  }
  formatLand(object) {
    const land = {
      currency: object[0],
      height: object[1],
      id: object[2]
    };
    return land;
  }
  getLands() {
    const {
      getLandColumnsCount,
      getLandRowsInColumn,
      getLandByCoordinates
    } = this.state.contract.methods;
    return getLandColumnsCount()
      .call()
      .then(count => {
        let array = [];
        for (let i = 0; i < count; i++) {
          array.push(i);
        }
        return array;
      })
      .then(columns =>
        Promise.all(columns.map(i => getLandRowsInColumn(i).call()))
      )
      .then(rowOfColumnCounts =>
        rowOfColumnCounts.map(heightOfColumn => {
          let columnArray = [];
          for (let j = 0; j < heightOfColumn; j++) {
            columnArray.push(j);
          }
          return columnArray;
        })
      )
      .then(gameBoardArray =>
        Promise.all(
          gameBoardArray.map((columnArray, columnIndex) =>
            Promise.all(
              columnArray.map(rowIndex =>
                getLandByCoordinates(columnIndex, rowIndex)
                  .call()
                  .then(f => this.formatLand(f))
              )
            )
          )
        )
      )
      .then(lands => this.setState({ lands }));
  }
  getFighters() {
    const {
      getFighterColumnsCount,
      getFighterRowsInColumn,
      getFighterByCoordinates
    } = this.state.contract.methods;
    // get amount of columns
    // for each column, get amount of rows
    // build promise array
    return getFighterColumnsCount()
      .call()
      .then(count => {
        let array = [];
        for (let i = 0; i < count; i++) {
          array.push(i);
        }
        return array;
      })
      .then(columns =>
        Promise.all(columns.map(i => getFighterRowsInColumn(i).call()))
      )
      .then(rowOfColumnCounts =>
        rowOfColumnCounts.map(heightOfColumn => {
          let columnArray = [];
          for (let j = 0; j < heightOfColumn; j++) {
            columnArray.push(j);
          }
          return columnArray;
        })
      )
      .then(gameBoardArray =>
        Promise.all(
          gameBoardArray.map((columnArray, columnIndex) =>
            Promise.all(
              columnArray.map(rowIndex =>
                getFighterByCoordinates(columnIndex, rowIndex)
                  .call()
                  .then(f => this.formatFighter(f))
              )
            )
          )
        )
      )
      .then(fighters => this.setState({ fighters }));
  }
  handleAlterFighter(id, top, right, bottom, left) {
    console.log("trying", id, top, right, bottom, left);
    this.state.contract.methods
      .alterFighter(id, top, right, bottom, left)
      .send({ from: this.state.accounts[0] })
      .then(r => this.refreshData());
  }
  handleAlterLand(addHeight, fighterOnLand) {
    let { x, y } = fighterOnLand;
    x = Number(x);
    y = Number(y);
    const { lands } = this.state;
    console.log("lands", x, y, lands);
    const alteredLand = lands[x][y];
    const newHeight = alteredLand.height + addHeight ? 1 : -1;
    const aboveLand = lands[x] && lands[x][y - 1];
    const belowLand = lands[x] && lands[x][y + 1];
    const leftLand = lands[x + 1] && lands[x + 1][y];
    const rightLand = lands[x - 1] && lands[x - 1][y];
    const landsArray = [aboveLand, belowLand, leftLand, rightLand];
    console.log(landsArray);
    const possible = landsArray.filter(
      land =>
        land &&
        land.height &&
        Number(land.height) - newHeight >= -1 &&
        Number(land.height) - newHeight <= 1
    );
    const landWithinOneHeightId = possible[0] && possible[0].id;
    console.log(
      "sending: ",
      addHeight,
      fighterOnLand.id,
      landWithinOneHeightId
    );
    this.state.contract.methods
      .alterLand(addHeight, fighterOnLand.id, landWithinOneHeightId)
      // .call()
      .send({
        from: this.state.accounts[0],
        gas: 5000000
      })
      .then(() => this.refreshData())
      .catch(e => alert(e.message));
  }
  handleFight(id, direction) {
    this.state.contract.methods
      .fight(id, direction)
      .send({
        from: this.state.accounts[0],
        gas: 5000000
      })
      .then(r => this.refreshData())
      .catch(e => alert(e.message));
  }
  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts , and contract...</div>;
    }
    const gBoard = this.gameBoard.current;
    // if (gBoard) {
    // console.log("offsetHeight", gBoard.offsetHeight);
    // console.log("offsetLeft", gBoard.offsetLeft);
    // console.log("offsetTop", gBoard.offsetTop);
    // console.log("offsetWidth", gBoard.offsetWidth);
    // console.log("getBoundingClientRect", gBoard.getBoundingClientRect());
    // }
    return (
      <div className="App">
        <SideBar
          show={!!this.state.alteringFighter || !!this.state.alteringLand}
        >
          {this.state.alteringFighter && (
            <AlterFighter
              handleAlterFighter={(id, top, right, bot, left) =>
                this.handleAlterFighter(id, top, right, bot, left)
              }
              {...this.state.alteringFighter}
            />
          )}
          {this.state.alteringLand && (
            <AlterLand
              handleAlterLand={addHeight =>
                this.handleAlterLand(
                  addHeight,
                  this.state.fighterOnAlteringLand
                )
              }
              {...this.state.alteringLand}
            />
          )}
        </SideBar>

        <h1>Blockplain</h1>
        <p>There are {this.state.accounts.length} accounts.</p>
        <div style={{ height: "100px", width: "100px", position: "relative" }}>
          <button
            onClick={() => this.setState({ rotateX: this.state.rotateX + 10 })}
            style={{ position: "absolute", left: "38%", top: "0" }}
          >
            U
          </button>
          <button
            onClick={() => this.setState({ rotateX: this.state.rotateX - 10 })}
            style={{ position: "absolute", left: "38%", bottom: "0" }}
          >
            D
          </button>
          <button
            onClick={() => this.setState({ rotateZ: this.state.rotateZ + 10 })}
            style={{ position: "absolute", top: "38%", left: "0" }}
          >
            L
          </button>
          <button
            onClick={() => this.setState({ rotateZ: this.state.rotateZ - 10 })}
            style={{ position: "absolute", top: "38%", right: "0" }}
          >
            R
          </button>
        </div>
        <BoardContainer>
          <GameBoard
            style={{
              transform: `rotateX(${this.state.rotateX}deg) 
            rotateY(${this.state.rotateY}deg) 
            rotateZ(${this.state.rotateZ}deg)`
            }}
            ref={this.gameBoard}
            rotateX={this.state.rotateX}
            rotateY={this.state.rotateY}
            rotateZ={this.state.rotateZ}
            translateX={this.state.translateX}
            translateY={this.state.translateY}
            translateZ={this.state.translateZ}
          >
            {this.state.fighters.map((arrayOfFighters, xCoord) => {
              return (
                <div key={"row" + xCoord}>
                  {arrayOfFighters.map((f, yCoord) => {
                    const { lands } = this.state;
                    let land = lands && lands[xCoord] && lands[xCoord][yCoord];
                    return (
                      <Cube
                        focusedLandCoordinates={
                          this.state.focusedLandCoordinates
                        }
                        onFocusClick={() => {
                          console.log("clicked");
                          this.setState({
                            focusedLandCoordinates: { x: xCoord, y: yCoord }
                          });
                        }}
                        cubeX={this.state.cubeX}
                        cubeY={this.state.cubeY}
                        cubeZ={this.state.cubeZ}
                        handleAlterFighterClick={() =>
                          this.setState({
                            alteringLand: false,
                            alteringFighter:
                              this.state.alteringFighter &&
                              this.state.alteringFighter.id === f.id
                                ? false
                                : f
                          })
                        }
                        handleAlterLandClick={() =>
                          this.setState({
                            alteringFighter: false,
                            alteringLand:
                              land &&
                              land.id &&
                              this.state.alteringLand &&
                              this.state.alteringLand.id === land.id
                                ? false
                                : land,
                            fighterOnAlteringLand: f
                          })
                        }
                        ownerFighterIndex={this.state.myFighters.indexOf(
                          this.state.myFighters.find(
                            f =>
                              Number(f.x) === xCoord && Number(f.y) === yCoord
                          )
                        )}
                        onFight={dir => this.handleFight(f.id, dir)}
                        key={"fighter" + xCoord + "" + yCoord}
                        fighter={f}
                        land={land}
                      />
                    );
                  })}
                  <OpenSquare
                    cubeX={this.state.cubeX}
                    cubeY={this.state.cubeY}
                    onClick={() =>
                      this.handleCreateFighter(xCoord, arrayOfFighters.length)
                    }
                    xCoordinate={xCoord}
                    yCoordinate={arrayOfFighters.length}
                  />
                </div>
              );
            })}
            {this.state.fighters[0] && this.state.fighters[0].length > 0 && (
              <OpenSquare
                cubeX={this.state.cubeX}
                cubeY={this.state.cubeY}
                onClick={() =>
                  this.handleCreateFighter(this.state.fighters.length, 0)
                }
                xCoordinate={this.state.fighters.length}
                yCoordinate={0}
              />
            )}
          </GameBoard>
        </BoardContainer>
      </div>
    );
  }
}

export default App;
