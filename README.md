# Blockplain

## Dependencies

1. Nodejs
2. Trufflejs
3. Ganache

## Get it running

1. Start ganache.
2. `truffle compile`
3. Make sure Ganache is running.
4. `truffle migrate --reset`
5. `cd client`
6. `npm start`

## Game Notes

- square shape
- each new fighter adds a new square
- attack once per day

# Player Choices

1. Fight

- must have a side that is equal or higher than side you are attacking
- when you win, you take over that land, and the loser switches places
- you recieve some money
- gain a "change" point to alter your fitherin the future

2. Alter Land

- higher = +1 to all sides
- lower = -1 to all sides

3. Alter Fighter

- change side numbers equal to "change" points a player has

# inspirations

https://www.google.com/imgres?imgurl=https%3A%2F%2Fi.gifer.com%2FE11.gif&imgrefurl=https%3A%2F%2Fgifer.com%2Fen%2F632&docid=gVHwS4zGssNOHM&tbnid=U6M3aVbniLq7lM%3A&vet=12ahUKEwiI9Jv0983eAhVpqVQKHbI9DA44yAEQMygeMB56BAgBEB8..i&w=480&h=400&bih=728&biw=1440&q=cartoon%20rock&ved=2ahUKEwiI9Jv0983eAhVpqVQKHbI9DA44yAEQMygeMB56BAgBEB8&iact=mrc&uact=8#h=400&imgdii=U6M3aVbniLq7lM:&vet=12ahUKEwiI9Jv0983eAhVpqVQKHbI9DA44yAEQMygeMB56BAgBEB8..i&w=480

https://www.dreamstime.com/stock-illustration-rock-stone-cartoon-flat-style-set-different-boulders-grass-vector-illustration-image75074984
