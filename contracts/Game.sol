pragma solidity ^0.4.24;
contract Game {
    struct Fighter{
        uint currency;
        int8 left;
        int8 right;
        int8 top;
        int8 bottom;
        address owner;
        uint unspentWins;
    }
    struct Land{
        uint currency;
        int height;
    }
    event FighterAdded (
        uint xCoordindate, 
        uint yCoordinate, 
        uint curency, 
        int8 left, 
        int8 right, 
        int8 top,
        int8 bottom
    );
    
    // only one fighter should be on any given x-y combination  
    // only one land should be in any given x-y combination
    mapping (uint => uint) public landIndexToXCoordinate;
    mapping (uint => uint) public landIndexToYCoordinate;
    mapping (uint => uint) public  fighterIndexToXCoordinate; 
    mapping (uint => uint) public fighterIndexToYCoordinate;
    mapping (uint => address) public fighterIndexToOwner;
    mapping (address => uint[])  public ownerToFighterIDs;
    // gameBoards will keep track of the fighter/land ID's 
    // and place them where they would be on a gameBoard
    // if the gameBoard were a 2d chess board
    uint[][] public gameBoardByLand;
    uint[][] public gameBoardByFighters;
    // use array to iterate through for constant functions?
    Land[] lands;
    Fighter[] fighters;
    
    uint landHeightUpgradeCost = 2;

    constructor() public {
        uint[] memory emptyArrayAsColumn;
        gameBoardByFighters.push(emptyArrayAsColumn);
        gameBoardByLand.push(emptyArrayAsColumn);
    }

    function createFighter (uint _xCoord, uint _yCoord) public {
  
        // x coordinate can only be farther by 1 than another land/fighter
        // so that there is no empty space between the farthest land/fighter
        // and its closest land/fighter on the x axis
        if (gameBoardByFighters.length < _xCoord || gameBoardByLand.length < _xCoord) revert("New land must be parellel with another column.");
        // if the new land/fighter is going to need a new columns make one
        if (gameBoardByFighters.length == _xCoord && gameBoardByLand.length == _xCoord){
            uint[] memory emptyArrayAsColumn;
            gameBoardByFighters.push(emptyArrayAsColumn);
            gameBoardByLand.push(emptyArrayAsColumn);
        }
        // land/fighter must not already exist at the exact x y coordinates
        // y coordinate must be higher by 1 than last index in y array
        if (gameBoardByFighters[_xCoord].length != _yCoord 
        || gameBoardByLand[_xCoord].length != _yCoord) revert("New land must be connected to a column.");
        
        // land and a fighter are created at the same time
        uint fighterID = fighters.push(Fighter(10, 5, 5, 5, 5, msg.sender, 0)) - 1;
        uint landID = lands.push(Land(0, 0)) - 1;
        
        // save the given coordinates of the new land/fighter
        landIndexToXCoordinate[landID] = _xCoord;
        landIndexToYCoordinate[landID] = _yCoord;
        fighterIndexToXCoordinate[fighterID] = _xCoord;
        fighterIndexToYCoordinate[fighterID] = _yCoord;
        // push the id onto each land/fighter gameBoard
        gameBoardByLand[_xCoord].push(landID);
        gameBoardByFighters[_xCoord].push(fighterID);
        
        // save the owner of the fighter for quick reference
        fighterIndexToOwner[fighterID] = msg.sender;
        // save the fighter id to the owners array
        ownerToFighterIDs[msg.sender].push(fighterID);
        // add the fighter to the first row, at the end
        emit FighterAdded(_xCoord, _yCoord, 10, 5, 5, 5, 5);

       
    }
    function getFighterByCoordinates(uint _x, uint _y)public view returns (uint, int8, int8, int8, int8, uint, uint, uint, uint){
        uint id = gameBoardByFighters[_x][_y];
        Fighter memory fighter = fighters[id];
        return (fighter.currency, fighter.left, fighter.right, fighter.top, fighter.bottom, 
        fighterIndexToXCoordinate[id], fighterIndexToYCoordinate[id], id, fighter.unspentWins);
    }
    
    function getLandByCoordinates(uint _x, uint _y) public view returns (uint, int, uint){
        uint id = gameBoardByLand[_x][_y];
        Land memory land = lands[id];
        return (land.currency, land.height, id);
    }
    
    function alterFighter(uint id, int8 _top, int8 _right, int8 _bottom, int8 _left) public{
        // verify the fighter is owned by the owner
        if (fighters[id].owner != msg.sender) revert("You do not own this fighter.");
         // verify total stats cannot be above 20 
        if (_left + _right + _top + _bottom != 20) revert("Too many stats to change.");
        int8 leftChange = fighters[id].left - _left;
        int8 rightChange = fighters[id].right - _right;
        int8 topChange = fighters[id].top - _top;
        int8 bottomChange = fighters[id].bottom - _bottom;
       
        int8 greatestPositiveChange = 0;
        int8 greatestNegativeChange = 0;
        if (greatestPositiveChange < leftChange) greatestPositiveChange = leftChange;
        if (greatestPositiveChange < rightChange) greatestPositiveChange = rightChange;
        if (greatestPositiveChange < topChange) greatestPositiveChange = topChange;
        if (greatestPositiveChange < bottomChange) greatestPositiveChange = bottomChange;
        if (greatestNegativeChange > leftChange) greatestNegativeChange = leftChange;
        if (greatestNegativeChange > rightChange) greatestNegativeChange = rightChange;
        if (greatestNegativeChange > topChange) greatestNegativeChange = topChange;
        if (greatestNegativeChange > bottomChange) greatestNegativeChange = bottomChange;

        int greatestChange = greatestPositiveChange;
        if (greatestNegativeChange * -1 > greatestPositiveChange) greatestChange = greatestNegativeChange * 1;
        int unspentWins = int(fighters[id].unspentWins);

        if (greatestChange > unspentWins) revert("Any single stat cannot change more than unspentWins.");
        fighters[id].left = _left;
        fighters[id].right = _right;
        fighters[id].top = _top;
        fighters[id].bottom = _bottom;
        fighters[id].unspentWins -= uint(greatestChange);
    }

    function getFighterCountByOwner() public view returns (uint){
        return ownerToFighterIDs[msg.sender].length;
    }
    function getFighterByOwnerListIndex(uint _index) public view returns (uint, uint , uint){
        uint id = ownerToFighterIDs[msg.sender][_index];
        uint xCoord = fighterIndexToXCoordinate[id];
        uint yCoord = fighterIndexToYCoordinate[id];
        return (xCoord, yCoord, id);
    }
    function getLandColumnsCount() public view returns (uint){
        return gameBoardByLand.length;
    }
    function getLandRowsInColumn(uint _column) public view returns (uint){
        return gameBoardByLand[_column].length;
    }
    function getFighterColumnsCount() public view returns (uint){
        return gameBoardByFighters.length;
    }
    function getFighterRowsInColumn(uint _column) public view returns (uint){
        return gameBoardByFighters[_column].length;
    }
    function fight(uint id, int8 _direction) public {
        uint attackerID = id;
        if (fighterIndexToOwner[attackerID] != msg.sender) revert("Not owners fighter.");
        uint xCoord = fighterIndexToXCoordinate[attackerID];
        uint yCoord = fighterIndexToYCoordinate[attackerID];
        Fighter memory attacker = fighters[attackerID];
        uint defenderID;
        int8 defenderSide;
        int8 attackerSide;
        uint defenderXCoord;
        uint defenderYCoord;
        uint defendingLandId;
        if (_direction == 0){ // top
            defenderXCoord = xCoord;
            defenderYCoord = yCoord - 1;
            defenderID = gameBoardByFighters[defenderXCoord][defenderYCoord]; // top
            defenderSide = fighters[defenderID].bottom;
            attackerSide = attacker.top;
            defendingLandId = gameBoardByLand[defenderXCoord][defenderYCoord];
        } else if (_direction == 1){ // right
            defenderXCoord = xCoord + 1;
            defenderYCoord = yCoord;
            defenderID = gameBoardByFighters[defenderXCoord][defenderYCoord]; // right
            defenderSide = fighters[defenderID].left;
            attackerSide = attacker.right;
            defendingLandId = gameBoardByLand[defenderXCoord][defenderYCoord];
        } else if (_direction == 2){ // bottom
            defenderXCoord = xCoord;
            defenderYCoord = yCoord + 1;
            defenderID = gameBoardByFighters[defenderXCoord][defenderYCoord]; // bottom
            defenderSide = fighters[defenderID].top;
            attackerSide = attacker.bottom;
            defendingLandId = gameBoardByLand[defenderXCoord][defenderYCoord];
        } else if (_direction == 3) { // left
            defenderXCoord = xCoord - 1;
            defenderYCoord = yCoord;
            defenderID = gameBoardByFighters[defenderXCoord][defenderYCoord]; // left
            defenderSide = fighters[defenderID].right;
            attackerSide = attacker.left;
            defendingLandId = gameBoardByLand[defenderXCoord][defenderYCoord];
        }
        int attackingLandHeight = lands[gameBoardByLand[xCoord][yCoord]].height;
        int heightDifference = attackingLandHeight - lands[defendingLandId].height;
        if (attackerSide + heightDifference < defenderSide) revert("Attacker is too weak");
        // // switch places
        gameBoardByFighters[xCoord][yCoord] = defenderID;
        gameBoardByFighters[defenderXCoord][defenderYCoord] = attackerID;
        fighterIndexToXCoordinate[defenderID] = xCoord;
        fighterIndexToYCoordinate[defenderID] = yCoord;
        fighterIndexToXCoordinate[attackerID] = defenderXCoord;
        fighterIndexToYCoordinate[attackerID] = defenderYCoord;
        // // defender loses currency
        // // attacker gains currency
        uint battleCost = 2;
        if (fighters[defenderID].currency < 2){
            battleCost = fighters[defenderID].currency;
        }
        fighters[defenderID].currency -= battleCost;
        fighters[attackerID].currency += 1;
        fighters[attackerID].unspentWins += 1;
        // // land where defender lost gains currency
        lands[defendingLandId].currency = lands[defendingLandId].currency + 1;
    }
    function alterLand(bool _addHeight, uint _fighterId, uint _within1LandId) public {
        if (fighterIndexToOwner[_fighterId] != msg.sender) revert("Not your fighter.");
        uint xCoord = fighterIndexToXCoordinate[_fighterId];
        uint yCoord = fighterIndexToYCoordinate[_fighterId];
        uint landID = gameBoardByLand[xCoord][yCoord];
        // verify the land has enough currency
        if (lands[landID].currency < landHeightUpgradeCost) revert("Not enough land currency.");
        int newHeight;
        if (_addHeight == true){
            newHeight = lands[landID].height + 1;
        } else {
            newHeight = lands[landID].height - 1;
        }
        // verify it is connected to land that is being altered
        uint closeX = landIndexToXCoordinate[_within1LandId];
        uint closeY = landIndexToYCoordinate[_within1LandId];
        uint total = 10 + xCoord - closeX + yCoord - closeY;
        if (total < 9 || total > 11 || total == 10) revert("Close land isn't close enough.");
        // verify suggested close land will have a height that is good enough
        int heightDifference = lands[_within1LandId].height - newHeight;
        if (heightDifference > 1 || heightDifference < -1) revert("Close land isn't close enough in height.");
        lands[landID].height = newHeight;
    }
}